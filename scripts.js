//Omlouvam se za czenglish.....O_o

let db = null;
//TODO EDIT DELETE bet , login vice uzivatelu??
//graf zisku v case
//sloupec zisk v radku tabulce 
//teoreticky pridat i datum



function createIndexedDB(){
        const request = window.indexedDB.open("betsDB");
        request.onupgradeneeded = e => {
            db = e.target.result;
            //const bets = db.createObjectStore("bets", {keyPath: "id"})
            const bets = db.createObjectStore("bets", {autoIncrement: true})
        }
        request.onsuccess = e => {
            db = e.target.result
            //alert(db.name);
            //alert(`success is called database name: ${db.name} version : ${db.version}`)
        }
        request.onerror = e => {
            alert(`error: ${e.target.error} was found `)
        }
        
}

function addBet(datum, zapas, tip, kurz, sk, vklad, vysledek){
    const bet = {
        datum: datum,
        zapas: zapas,
        tip: tip,
        kurz: kurz,
        sk: sk,
        vklad: vklad,
        stav: vysledek
    }
    
    const tx = db.transaction("bets", "readwrite");
    const bets = tx.objectStore("bets");
    bets.add(bet);
    
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function addDummyBets(count){
	
	var stavy = ["ok", "ko", "void", ""];
	var dd = 10;
	for(var i = 0; i < count; i++){
		
		/*var mm = parseInt(getRandomArbitrary(1,12));
		var dd = parseInt(getRandomArbitrary(1,28));
		*/
		
		//var datum = "2019-" + mm + "-" + dd;
		var datum = "2019-10-" + (dd++);
		var zapas = "Zapas " + parseInt(getRandomArbitrary(1,1000));
		var tip = parseInt(getRandomArbitrary(0,2));
		var kurz = getRandomArbitrary(1,2.5).toFixed(2);
		var sk = "SK " + parseInt(getRandomArbitrary(1,5));
		var vklad = parseInt(getRandomArbitrary(1,1000));
		var stav = stavy[parseInt(getRandomArbitrary(0,5))];
		
		addBet(datum, zapas, tip, kurz, sk, vklad, stav);
	}
    /*addBet("2020-01-13","A B", 1, 2.5, "Tipsport", 1000, "ok");
    addBet("2020-01-14","A B", 1, 1.5, "Tipsport", 1000, "ko");
    addBet("2020-01-15","A B", 1, 2, "Tipsport", 1000, "ok");
    addBet("2020-01-16","A B", 1, 11, "Tipsport", 1000, "ok");
    addBet("2020-01-17","A B", 1, 5, "Tipsport", 1000, "void");
    addBet("2020-01-18","A B", 1, 1.6, "Tipsport", 1000, "");
	*/
}

function readBets(){
    
    const tx = db.transaction("bets","readonly") ;
    const bets = tx.objectStore("bets");
    const request = bets.openCursor() ;
    
    //get rid of new bet form if present
    $("#bets").empty();
    $("#graph").empty();
    $("#bets").append(
        '<table id="betsTable" class="tableizer-table">' + 
        '<thead><tr class="tableizer-firstrow"><th>#</th><th>Datum</th><th>Zápas</th><th>Tip</th><th>Kurz</th><th>Sázková kancelář</th><th>Vklad</th><th>Stav</th><th></th></tr></thead><tbody>'
                
    );
    
    request.onsuccess = e => {
      const cursor = e.target.result
  
      if (cursor) {        
        //row will have ID 'bet-XY' and class-defined by state
        $("#betsTable > tbody:last-child" ).append('<tr id=bet-' + `${cursor.key}` + ' class =' + `${cursor.value.stav}` + '><td>' + `${cursor.key}` + '</td>' +
          '<td>' + `${cursor.value.datum}` + '</td>' +
          '<td>' + `${cursor.value.zapas}` + '</td>' + 
          '<td>' + `${cursor.value.tip}` + '</td>' + 
          '<td>' + `${cursor.value.kurz}` + '</td>' +
          '<td>' + `${cursor.value.sk}` + '</td>' +
          '<td>' + `${cursor.value.vklad}` + '</td>' +
          '<td><select class="betSelect" id="bet-select-' + `${cursor.key}` + '"><option value = "unknown"></option><option value = "ok">OK</option><option value="ko">KO</option><option value = "void">VOID</option></select></td>' +
		  //'<td> <a href="#" id="remove-bet-' + `${cursor.key}` + '"> X </a> </td></tr>'
		  //'<td> <a href="#" class="remove-bet`"> X </a> </td></tr>'
		  "<td> <input type='button' class='remove-bet' value='X'> </td></tr>"
		  
        );
        //set correct values for bet State SELECTs
        var betselect = "#bet-select-" + `${cursor.key}`;
        var betState = `${cursor.value.stav}`;
        
        //console.log(betState);
        $(betselect).val(betState);

        cursor.continue();
      }
    }
    
	printStats();
	printDateProfit();
	
    //printGraph(1,2,3);
}

function printOKGraph(OK, KO, VOID){
    //https://gionkunz.github.io/chartist-js/examples.html#simple-pie-chart
    //
    var code = "";
    //code += '<div class="ct-chart ct-perfect-fourth"><\/div><script>';
    code += '<script>';

    code += "var data = {";
    code += "  labels: ['OK', 'KO', 'VOID'],";
    code += "  series: [" +  OK + "," + KO + "," + VOID + "]";
    code += "};";
    code += "";
    code += "var options = {";
    code += "  labelInterpolationFnc: function(value) {";
    code += "    return value[0]";
    code += "  }";
    code += "};";
    code += "";
    code += "var responsiveOptions = [";
    code += "  ['screen and (min-width: 640px)', {";
    code += "    chartPadding: 30,";
    code += "    labelOffset: 100,";
    code += "    labelDirection: 'explode',";
    code += "    labelInterpolationFnc: function(value) {";
    code += "      return value;";
    code += "    }";
    code += "  }],";
    code += "  ['screen and (min-width: 1024px)', {";
    code += "    labelOffset: 80,";
    code += "    chartPadding: 20";
    code += "  }]";
    code += "];";
    code += "";
    code += "new Chartist.Pie('#pieChart', data, options, responsiveOptions);";
    code += "<\/script>"
    console.log(code);
    $("#pieChart").addClass("ct-perfect-fourth");
    $("#pieChart").html(code);
    
}

function updateBetState(index, state){
    //todo state by mel obsahovat jen urcitou mmnozinu, ne cokoliv - ale to je zajistene tim co tam posilam - ale osetrit by se dalo
    const tx = db.transaction("bets","readwrite") ;
    const bets = tx.objectStore("bets");
    const request = bets.get(index);
    
    console.log(index + " " + state);
    request.onsuccess = function(){
        var data = request.result;
        data.stav = state;
        var requestUpdate = bets.put(data,index);
        requestUpdate.onsuccess = function(){
            console.log("bet updated");
        }
        requestUpdate.onerror = function(){
            console.log("bet not updated");
        }
    }
    //reload bets table - this is probably not the best solution, because the whole table is reloaded, but I will not upgrade it now
    readBets();
}

function removeBet(index){
	const tx = db.transaction("bets","readwrite") ;
    const bets = tx.objectStore("bets");
    const request = bets.delete(index);
	
	request.onsuccess = function(){
		//alert("Bet deleted");
	}
	readBets();
}

function printStats(){
    const tx = db.transaction("bets","readonly") ;
    const bets = tx.objectStore("bets");
    const request = bets.openCursor() ;

    var OKCount = 0, KOCount = 0 , VOIDCount = 0, UNSETTLEDCount = 0;
    var profit = 0;

    request.onsuccess = e => {
     const cursor = e.target.result
    
     if(cursor){
        var stav = `${cursor.value.stav}`;
        var vklad =  parseInt(`${cursor.value.vklad}`);
        var kurz =  parseFloat(`${cursor.value.kurz}`);
        
        switch(stav){
            case "ok":
                OKCount++;
                profit += (kurz-1)*vklad;
                console.log("kurz: " + kurz + "vklad: " + vklad);
                break;
            case "ko":
                KOCount++;          
                profit -= vklad;
                break;
            case "void":
                VOIDCount++;
                break;
            default: 
                UNSETTLEDCount++;                                 
        }        
        cursor.continue();
        
     }else{
        //asi tady musim vracet
         $("#bets").prepend("<p style='text-align: center'><b>OK Tipů: </b>" + OKCount + " <b>&nbsp;&nbsp;&nbsp;&nbsp;KO Tipů: </b>" + KOCount + 
         " <b>&nbsp;&nbsp;&nbsp;&nbsp;VOID Tipů: </b>" + VOIDCount + 
         "<br> <b>Zisk: </b>" + profit.toFixed(2) + "</p>"  
         );      
         printOKGraph(OKCount,KOCount,VOIDCount);   
     }         
    }
    /*$("#bets").append(OKCount);
    request.oncomplete = e => {
        //console.log("s" + OKCount + " " + KOCount + " " + VOIDCount + " " + UNSETTLEDCount + " " + profit);
        alert("FINISH");
    }
    request.onerror = e => {
        //console.log("s" + OKCount + " " + KOCount + " " + VOIDCount + " " + UNSETTLEDCount + " " + profit);
        alert("ERROR");
    }*/
}

function printDateProfit(){
    const tx = db.transaction("bets","readonly") ;
    const bets = tx.objectStore("bets");
    const request = bets.openCursor() ;

	//kazdy zaznam ulozim do pole jako datum-profit a tyto zaznamy pak zpracuju jinde, protoze data nebudou duplicitni abude jednodussi to delat s polem nez s kurzorem
    let data = [], zisky = [];
	let index = 0;
    request.onsuccess = e => {
     const cursor = e.target.result
    
     if(cursor){
        var datum = `${cursor.value.datum}`;
        data[index] = datum;                     
        
		var stav = `${cursor.value.stav}`;
        var vklad =  parseInt(`${cursor.value.vklad}`);
        var kurz =  parseFloat(`${cursor.value.kurz}`);
        var profit = 0;
		
        switch(stav){
            case "ok":                
                profit = (kurz-1)*vklad;                
                break;
            case "ko":                
                profit =  - vklad;
                break;
            case "void":             
                break;
            default: 
                
        } 
		zisky[index] = profit;
		index++;
        cursor.continue();
        
     }else{
        //asi tady musim vracet
        printLineGraph(data, zisky);
     }         
    }
    /*$("#bets").append(OKCount);
    request.oncomplete = e => {
        //console.log("s" + OKCount + " " + KOCount + " " + VOIDCount + " " + UNSETTLEDCount + " " + profit);
        alert("FINISH");
    }
    request.onerror = e => {
        //console.log("s" + OKCount + " " + KOCount + " " + VOIDCount + " " + UNSETTLEDCount + " " + profit);
        alert("ERROR");
    }*/
}

function deepCopyArray(array){
	let res = [];
	for(var i = 0; i < array.length; i++){
		res[i] = array[i];
	}
	return res;
}

function printLineGraph(data, zisky){
	//zisky za jednotlive dny - datumy v IDB ale nemusi byt poporade, proto slozitejsi reseni
	
	dataSorted = deepCopyArray(data);
	dataSorted.sort();
	var dataUniq = []; 
	var ziskyUniq = [];
	var ziskyRunningSum = [];
	
	for (var i=0; i < data.length; i++){
		if(dataSorted[i] == dataSorted[i+1]) continue; //each date I want to use only once
		var celkemZiskZaDen = 0;
		dataUniq[dataUniq.length] = dataSorted[i];
		for(var j=0; j < data.length; j++){ //asi staci jen do i, ale whatever
			if(data[j] == dataSorted[i]){
				celkemZiskZaDen += zisky[j]; //pridej vsechny zisky za dany den
			}	
		}
		ziskyUniq[ziskyUniq.length] = celkemZiskZaDen;
	}
	
	ziskyRunningSum[0] = zisky[0]; 
	for(var i = 1; i < ziskyUniq.length; i++){
		ziskyRunningSum[i] = ziskyRunningSum[i-1] + ziskyUniq[i];
	}
	console.log(ziskyRunningSum.toString());
	
	var labels = "labels: [", series = "[";
	//make string of labels and series for the graph
	for(var i = 0; i < dataUniq.length; i++){
		labels += "'" + dataUniq[i] + "'" ; 
		series += "" + ziskyRunningSum[i];
		if(i != dataUniq.length-1){
			labels += ", ";
			series += ", ";
		}		
	}
	
	labels += "],";
	series += "],";
	
	console.log(labels);
	console.log(series);
	
	var code = "";
    code += '<div class="ct-chart ct-perfect-fourth"><\/div><script>';
	code += "new Chartist.Line('#profitChart', {";
	//code += "  labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],";			
	code += labels;
	code += "  series: [";
	//code += "    [12, 9, 7, 8, 5,0,0],";
	code += series;
	code += "  ]";
	code += "}, {";
	code += "  fullWidth: true,";
	code += "  chartPadding: {";
	code += "    right: 40";
	code += "  }";
	code += "});";
	code += "<\/script>"
    console.log(code);
	$("#profitChart").addClass("ct-perfect-fourth");
    $("#profitChart").html(code);

}

function todayYYYYMMDD(){
//returns today date in YYYY-MM-DD format - for use in form
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    return today;
}

function convertDate(yyyymmdd){
    //convert yyyy-mm-dd to dd.mm.yyyy
    var dd = yyyymmdd.substring(6,2);
    var mm = yyyymmdd.substring(4,2);
    var yyyy = yyyymmdd.substring(0,4);
    return dd + '.' + mm + '.' + yyyy;
}

$(document).ready(function(){
        createIndexedDB();        
                
        //This functions creates dynamic form for bet adding
        $('#newbet').click(function(){
            $("#bets").empty();
            $("#pieChart").empty().removeClass("ct-perfect-fourth");
			$("#profitChart").empty().removeClass("ct-perfect-fourth");
			
			
            $('#bets').append(
              //I dont know if this is correct way to pass multiple lined html, because it is unreadable
               "<form id='newbetform'> <table> <tr> <td><label for='datum'>Datum</label></td> <td><input type='date' name='datum' tabindex='0' id='datum' value='" + todayYYYYMMDD() + "' /> </tr>" +  
               "<tr> <td><label for='zapas'>Zápas</label></td> <td><input type='text' name='zapas' tabindex='1' id='zapas' value='' /> </tr>" +
               "<tr> <td><label for='tip'>Tip</label></td> <td><input type='text' name='tip' tabindex='2' id='tip' value='' required /> </tr> " + 
               " <tr> <td><label for='kurz'>Kurz</label></td> <td><input type='number' name='kurz' tabindex='3' id='kurz' min='1' value='' /> </tr>" + 
               " <tr> <td><label for='sazkovka'>Sázková kancelář</label></td> <td> <select name='sazkovka' tabindex='4' id='sazkovka'> <option value='Fortuna'>Fortuna</option> <option value='Tipsport'>Tipsport</option> <option value='Synot'>Synottip</option> <option value='Sazka'>Sazka</option> <option value='Maxitip'>Maxitip</option> </select> </tr>"+
               " <tr> <td><label for='vklad'>Vklad</label></td> <td><input type='number' name='vklad' tabindex='5' id='vklad' min='1' value='' required /> </tr>"+
               " <tr> <td><label for='vysledek'>Výsledek</label></td> <td> <select name='vysledek' tabindex='6' id='vysledek'> <option value='unknown'></option> <option value='ok'>OK</option> <option value='ko'>KO</option> <option value='void'>VOID</option> </select> </tr> " +
               "<tr> <td><input type='button' id='formbutton' value='Submit'></td> </tr>" +
			   "<tr> <td><input type='button' id='addDummyBets' value='Add dummy bets'></td> </tr> </table> </form>" 
            );       
        });
        
      /*$(document).on('submit', '#newbetform', function(){
           alert('sent');
        }); 
      */
      $(document).on('click', '#formbutton', function(){
            var datum = $('#datum').val();
            var zapas = $('#zapas').val();
            var tip = $('#tip').val();
            var kurz = $('#kurz').val();
            var sk = $('#sazkovka').val();
            var vklad = $('#vklad').val();
            var vysledek = $('#vysledek').val();
            
            //addDummyBets();
            
            if(kurz == "" || vklad == ""){
                alert("Pole vklad a Kurz musí být vyplněny");
                return;
            }
            
            addBet(datum, zapas, tip, kurz, sk, vklad, vysledek);
            
            $('#newbetform').trigger('reset');
            alert('Bet added');            
      });
      
      $(document).on('change', '.betSelect', function(){
            //row element is the grandparent
            //first remove the class of the row and then set the newly selected one
            //possible problem would be if the row had another class I dont want to remove, but it is not the case now 
            $(this).parent().parent().removeClass();
            $(this).parent().parent().addClass($(this).val()); 
            //$('#newbetform').trigger('reset');            
            //TODO - Save current state to DB         
            //updateBetState(3, "Tessst" + Math.random());
            var index = $(this).parent().parent().find("td:first").text(); //first td in current row
            var newState = $(this).val();
            
            
            updateBetState(parseInt(index),newState);
            
      });

      $(document).on('click', '.remove-bet', function(){
            //row element is the grandparent
            
            //updateBetState(3, "Tessst" + Math.random());
            var index = parseInt($(this).parent().parent().find("td:first").text()); //first td in current row
            //alert("removing" + index);
			removeBet(index);
       
      });
      
      $("#history").click(function(){
        readBets(); //I dont know why I have to have it this way and not directly readBets as handler, but it causes exceptions
      }); 
	  
      $(document).on('click', '#addDummyBets', function(){
            
            addDummyBets(20);
            
            $('#newbetform').trigger('reset');
            alert('Dummy bets added');            
      });	  
	  
});



